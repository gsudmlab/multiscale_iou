## Multiscale IoU: A Metric for Evaluation of Salient Object Detection with Fine Structures

*Azim Ahmadzadeh* ([aahmadzadeh1@cs.gsu.edu](aahmadzadeh1@cs.gsu.edu)),
*Dustin J. Kempton*, *Yang Chen*, and *Rafal A. Angryk*

Department of Computer Science, Georgia State University

**Conference:** [IEEE ICIP 2021](https://www.2021.ieeeicip.org/)

**Paper (arXiv):** [https://arxiv.org/pdf/2105.14572.pdf](https://arxiv.org/pdf/2105.14572.pdf)


**Abstract:**
Increasingly, general-purpose object-detection algorithms are
being applied to detection tasks of singular focus, where their
generality poses some limitations. The coarse segmentation
of objects is one of these limitations, and can be traced back
to how we evaluate the performance of these algorithms. Recognizing
the value of this generality, and the limited use of
task-specific algorithms, our goal is to re-negotiate this tradeoff
and to close the gap between these two worlds. In this
work, we present a new metric that is a marriage of a popular
evaluation metric named Intersection over Union (IoU), and a
concept from fractal geometry called fractal dimension. Using
the ideas behind these concepts, we propose *Multiscale
IoU* (MIoU) which allows comparison of the detected and
ground-truth regions at multiple resolution levels. Borrowing
examples from domains such as Heliophysics and Botany, we
showcase the magnitude of insensitivity IoU exhibits when
quantifying the differences between two regions. Furthermore,
we conduct experiments on synthetic samples to show
that MIoU is indeed sensitive to the boundary structure of objects.
To further examine the reliability of MIoU, we show
that on a dataset of real-world objects, its values follow the
same distribution as those of IoU do.

---
### Poster:

[![fig_1](./_poster/ICIP_Poster.jpg)](https://dmlab.cs.gsu.edu/docs/icip2021/ICIP_poster_Jul_2021_v2.pdf)


### Walk-through:
To see all the experiments presented in the paper, see the following notebook:

 - [./_walk_through/experiments.ipynb](./_walk_through/experiments.ipynb),

---

Using the above-mentioned notebook, the following experiments
can be reproduced: 

![fig_1](./plots/3_examples_with_plots.svg)

**Figure 1.** *Examples from different domains showing the metrics IoU,
Precision, Recall, and F1-score fail to capture prominent differences
between the proposed regions, dt1 and dt2, when compared with the
ground-truth region, gt. Example A depicts the issue in its simplest form.
Example B and C illustrate the same issue using the mask of a solar filament
and the mask of a leaf sample of the Metasequoia Glyptostroboides tree.*

---
![fig_2](./plots/palette_example_ExpF.svg)

**Figure 2.** *Comparison of area-based metrics on the 28 estimates (shown on
top) for the ground-truth region, indexed 3-1.*

---

![fig_3](./plots/boxplots_categories_ExpX.svg)

**Figure 3.** *Comparison of distributions of IoU and MIoU on 2500 masks
obtained from five categories of COCO dataset.*


---
## Guide:


### Metrics:
The following metrics are implemented:

 - Precision & Recall: [./miou/metrics/precision_recall.py](miou/metrics/precision_recall.py),
 - IoU: [./miou/metrics/iou.py](miou/metrics/iou.py),
 - AIoU: [./miou/metrics/aiou.py](miou/metrics/miou.py).


### Segmentation:
The metric `MIoU` relies on multi-resolution, grid-based
segmentation of objects. For segmentation of each mask, see:

 - [segmentation.py](miou/utils/segmentation.py).


### Test Images:
The binary masks needed for the presented experiments are available here:

 - [./test_images](./test_images). 

---
### Requirements:
 - Python 3.8 
 - For a list of all required packages, see [./requirements.txt](./requirements.txt).
 - For running the notebook, it is best to use [JupyterLab](https://github.com/jupyterlab/jupyterlab).

---
### Cite this:
bibTeX:
```
@inproceedings{ahmadzadeh2021multiscale,
  title={Multiscale IoU: A Metric for Evaluation of Salient Object Detection with Fine Structures},
  author={Ahmadzadeh, Azim and Kempton, Dustin J. and Chen, Yang and Angryk, Rafal A.},
  booktitle={2021 IEEE International Conference on Image Processing (ICIP)},
  pages={},
  year={in press},
  organization={IEEE}
}
```
MLA:
```
Ahmadzadeh, Azim, Dustin J. Kempton, Yang Chen, and YongRafal A. Angryk. (in press) "Multiscale IoU: A Metric for Evaluation of Salient Object Detection with Fine Structures"
2021 IEEE International Conference on Image Processing (ICIP). IEEE, 2021.
```
